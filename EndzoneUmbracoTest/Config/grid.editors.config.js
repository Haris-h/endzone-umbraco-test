﻿[
	{
	    "name": "Rich text editor",
	    "alias": "rte",
	    "view": "rte",
	    "icon": "icon-article"
	},
	{
	    "name": "Image",
	    "alias": "media",
	    "view": "media",
	    "icon": "icon-picture"
	},
	{
	    "name": "Macro",
	    "alias": "macro",
	    "view": "macro",
	    "icon": "icon-settings-alt"
	},
	{
	    "name": "Embed",
	    "alias": "embed",
	    "view": "embed",
	    "icon": "icon-movie-alt"
	},
    {
        "name": "Headline",
        "alias": "headline",
        "view": "textstring",
        "icon": "icon-coin",
        "config": {
            //"style": "font-size: 36px; line-height: 45px; font-weight: bold",
            "markup": "<h1>#value#</h1>"
        }
    },
    {
        "name": "Headline 2",
        "alias": "headline2",
        "view": "textstring",
        "icon": "icon-coin",
        "config": {
            "markup": "<h2>#value#</h2>"
        }
    },
    {
        "name": "Headline 3",
        "alias": "headline3",
        "view": "textstring",
        "icon": "icon-coin",
        "config": {
            "markup": "<h3>#value#</h3>"
        }
    },
    {
        "name": "Paragraph",
        "alias": "paragraph",
        "view": "textstring",
        "icon": "icon-coin",
        "config": {
            "markup": "<p>#value#</p>"
        }
    },
	{
	    "name": "Quote",
	    "alias": "quote",
	    "view": "textstring",
	    "icon": "icon-quote",
	    "config": {
	        "style": "border-left: 3px solid #ccc; padding: 10px; color: #ccc; font-family: serif; font-variant: italic; font-size: 18px",
	        "markup": "<blockquote>#value#</blockquote>"
	    }
	}
]