$(document).ready( function () {
	// Fixed navigation
	// http://codereview.stackexchange.com/questions/49285/css-and-html5-for-this-fixed-header-on-scroll
	var $window = $(window);
	var nav = $('body');
	if ($window.scrollY >= 150) {
		nav.addClass('fixed-header');
	}
	$window.scroll(function(){
	    if ($window.scrollTop() >= 150) {
	       nav.addClass('fixed-header');
	    }
	    else {
	       nav.removeClass('fixed-header');
	    }
	});
	
	$('.megamenu').hover(
		function () {
			$(this).parent().addClass('hover');
		}, 
		function () {
			$(this).parent().removeClass('hover');
		}
	);	
	
	// Check availability widget
	$('#check-avail-toggle').on('click', function () {
		$('#check-avail-form').slideToggle();
		return false;
	});
		
	var today = new Date();
	var tomorrow = new Date();
	tomorrow.setDate( today.getDate() + 1 );

	$("#arrive").datepicker({
		minDate: new Date(),
		dateFormat: "dd/mm/yy",
		onSelect: function () {
    		// Desktop
		    var dt1 = $("#arrive").datepicker("getDate");
		    var dt2 = $("#depart").datepicker("getDate");
			if (dt1 > dt2) {
				var dt1b = dt1;
				dt1b.setDate( dt1.getDate() + 1 );
				$("#depart").datepicker("setDate", dt1b);
			}
			$("#depart").datepicker("option", "minDate", dt1);
		}
	});
	$("#depart").datepicker({
		dateFormat: "dd/mm/yy",
		minDate: $("#arrive").datepicker("getDate"),
		onClose: function () {
		    var dt1 = $("#arrive").datepicker("getDate");
		    var dt2 = $("#depart").datepicker("getDate");
			//check to prevent a user from entering a date below date of dt1
			if (dt2 <= dt1) {
			    var minDate = $("#depart").datepicker("option", "minDate");
			    $("#depart").datepicker("setDate", minDate);
			}
		}
    });
	
	
	
	
	// Heading keylines
	$('.main-content h1, .landing-item-text h2, .left-content h3, .one-column h3').each( function () {
		if ( $(this).text() != '' ) {
			$(this).after( '<div class="divider" style="width: ' + Math.floor($(this).width() * 0.7) + 'px;"></div>' );
		}
	});
	
	
	
	// Sliders 
	$('.main-slider').flexslider({
		directionNav: true,
		controlNav: false
	});
	
	
	
	
	
	// Homepage rollover content 
	$('.home-rollover, .home-center-rollover').on('mouseenter', function () {
		$(this).addClass('hover');
	});
	$('.home-rollover, .home-center-rollover').on('mouseleave', function () {
		$(this).removeClass('hover');
	});
	
	
	
	
	
	// Mobile
	$('#mobile-nav-open').on('click', function () {
    	$('#mobile-nav').show();
    	setTimeout( function () {
	    	$('#mobile-nav').addClass('open');
    	}, 100);
    	return false;
	});
	$('#mobile-nav-close').on('click', function () {
		$('#mobile-nav').removeClass('open');
		setTimeout( function () {
    		$('#mobile-nav').hide(); // Hide the menu when closed, otherwise it makes the page longer
        }, 500);
		return false;
	});
	
	$('#mobile-check-avail-toggle').on('click', function () {
		$('#mobile-check-avail-form').slideToggle();
		return false;
	});
	
	$("#mobile-arrive").datepicker({
		minDate: new Date(),
		dateFormat: "dd/mm/yy",
		onSelect: function (date) {
    		// Desktop
			var dt1 = $('#mobile-arrive').datepicker('getDate');
			var dt2 = $('#mobile-depart').datepicker('getDate');
			if (dt1 > dt2) {
				var dt1b = dt1;
				dt1b.setDate( dt1.getDate() + 1 );
				$('#mobile-depart').datepicker('setDate', dt1b);
			}
			$('#mobile-depart').datepicker('option', 'minDate', dt1);
		}
	});
	$('#mobile-depart').datepicker({
		dateFormat: "dd/mm/yy",
		minDate: $('#mobile-arrive').datepicker('getDate'),
		onClose: function () {
			var dt1 = $('#mobile-arrive').datepicker('getDate');
			var dt2 = $('#mobile-depart').datepicker('getDate');
			//check to prevent a user from entering a date below date of dt1
			if (dt2 <= dt1) {
				var minDate = $('#mobile-depart').datepicker('option', 'minDate');
				$('#mobile-depart').datepicker('setDate', minDate);
			}
		}
	});

	
	if (window.location.pathname.toLowerCase().indexOf('/our-offers') != -1) {
	    $(document).on('click', '.btnOffersPager', function () {
	        /*fetching ID for active class*/
	        var pageSize = $("#pageSize").val();
	        $.ajax({
	            url: '/Umbraco/surface/Offer/GetOffersByPageNumber',
	            success: function (result) {
	                $('#offersList').html(result);
	            },
	            type: 'POST',
	            dataType: 'text',
	            data: {
	                'pn': this.name,
	                'pageSize': pageSize
	            }
	        });
	    });
	}
	if (window.location.pathname.toLowerCase().indexOf('/contact-us') != -1) {
	    var overallTotalCost = 0;
	    var guestInfoTotalCost = 0;

        //General info section
	    var ctoday = new Date();
	    var ctomorrow = new Date();
	    ctomorrow.setDate(ctoday.getDate() + 1);

	    $("#arriveDate").datepicker({
	        minDate: new Date(),
	        dateFormat: "dd/mm/yy",
	        onSelect: function () {
	            // Desktop
	            var dt1 = $("#arriveDate").datepicker("getDate");
	            var dt2 = $("#departDate").datepicker("getDate");
	            if (dt1 > dt2) {
	                var dt1b = dt1;
	                dt1b.setDate(dt1.getDate() + 1);
	                $("#departDate").datepicker("setDate", dt1b);
	            }
	            $("#departDate").datepicker("option", "minDate", dt1);
	        }
	    });
	    $("#departDate").datepicker({
	        dateFormat: "dd/mm/yy",
	        minDate: $("#arriveDate").datepicker("getDate"),
	        onClose: function () {
	            var dt1 = $("#arriveDate").datepicker("getDate");
	            var dt2 = $("#departDate").datepicker("getDate");
	            //check to prevent a user from entering a date below date of dt1
	            if (dt2 <= dt1) {
	                var minDate = $("#departDate").datepicker("option", "minDate");
	                $("#departDate").datepicker("setDate", minDate);
	            }
	        }
	    });
	    $("#lastVisitDate").datepicker({
	        dateFormat: "dd/mm/yy"
	    });

	    function AddToGeneralInfoTotal (value) {
	        guestInfoTotalCost = parseFloat(guestInfoTotalCost) + parseFloat(value);
	        overallTotalCost = parseFloat(overallTotalCost) + parseFloat(value);
	        $("#GuestInfo_TotalCost").val(guestInfoTotalCost);
	        $("#Overall_TotalCost").val(overallTotalCost);
	    }
	    function RemoveFromGeneralInfoTotal(value) {
	        guestInfoTotalCost = parseFloat(guestInfoTotalCost) - parseFloat(value);
	        overallTotalCost = parseFloat(overallTotalCost) - parseFloat(value);
	        $("#GuestInfo_TotalCost").val(guestInfoTotalCost);
	        $("#Overall_TotalCost").val(overallTotalCost);
	    }

	    var previousValue;
	    $("#resort").on("change", function (e) {
	        if (guestInfoTotalCost > 0) {
	            guestInfoTotalCost -= previousValue;
	            overallTotalCost -= previousValue;
	        }
	        AddToGeneralInfoTotal(this.value);
	        previousValue = this.value;
	    });

	    $("#DietaryRequirements_Kosher").change(function () { // bind a function to the change event
            if ($(this).is(":checked")) { // check if the radio is checked
                AddToGeneralInfoTotal(50);
            }
	    });
	    $("#DietaryRequirements_Halal").change(function () {
	        if ($(this).is(":checked")) {
	            RemoveFromGeneralInfoTotal(50);
	        }
	    });
	    $("#DietaryRequirements_None").change(function () { 
	        if ($(this).is(":checked")) {
	            RemoveFromGeneralInfoTotal(50);
	        }
	    });
	    $("#Guest1_SlipperUpgradeYes").change(function () { 
	        if ($(this).is(":checked")) { 
	            AddToGeneralInfoTotal(2);
	        }
	    });
	    $("#Guest1_SlipperUpgradeNo").change(function () {
	        if ($(this).is(":checked")) {
	            RemoveFromGeneralInfoTotal(2);
	        }
	    });
	    $("#Guest2_SlipperUpgradeYes").change(function () {
	        if ($(this).is(":checked")) {
	            AddToGeneralInfoTotal(2);
	        }
	    });
	    $("#Guest2_SlipperUpgradeNo").change(function () {
	        if ($(this).is(":checked")) {
	            RemoveFromGeneralInfoTotal(2);
	        }
	    });
	    //End General info section

	    //Treatment section

	    var treatmentsTotalCost = 0;

	    function AddToTreatmentsTotal(value) {
	        treatmentsTotalCost = parseFloat(treatmentsTotalCost) + parseFloat(value);
	        overallTotalCost = parseFloat(overallTotalCost) + parseFloat(value);
	        $("#TreatmentsTotalCost").val(treatmentsTotalCost);
	        $("#Overall_TotalCost").val(overallTotalCost);
	    }
	    function RemoveFromTreatmentsTotal(value) {
	        treatmentsTotalCost = parseFloat(treatmentsTotalCost) - parseFloat(value);
	        overallTotalCost = parseFloat(overallTotalCost) - parseFloat(value);
	        $("#TreatmentsTotalCost").val(treatmentsTotalCost);
	        $("#Overall_TotalCost").val(overallTotalCost);
	    }


	    $("#btnAddTreatmentRow").on("click", function() {
	        $('#treatmentTable > tbody:last-child').append("<tr>" +
	            '<td class="gf-table-m"><input type="text" name="Treatment_GuestName" /></td>' +
	            '<td class="gf-table-l"><input type="text" name="Treatment_Treatment" /></td>' +
	            '<td class="gf-table-date"><input type="text" class="date" name="Treatment_Date" /></td>' +
	            '<td class="gf-table-s"><input type="text" name="Treatment_AMPM" /></td>' +
	            '<td class="gf-table-s">' +
	                '<select name="Treatment_Therapist">' +
	                    '<option value="Either">Either</option>' +
	                    '<option value="Female">Female</option>' +
	                    '<option value="Male">Male</option>' +
	                '</select>' +
	            '</td>' +
	            '<td class="gf-table-s"><input type="text" name="Treatment_Cost" data-costgroup="Treatment" /></td>' +
	         "</tr>");
	    });

	    var treatmentFocusValue;
	    $(document).on('change', 'input[name="Treatment_Cost"]', function () {
	        alert(this.value);
	        if (treatmentFocusValue != "" && treatmentFocusValue != undefined && this.value != "" && this.value != undefined) {
	            RemoveFromTreatmentsTotal(treatmentFocusValue);
	            AddToTreatmentsTotal(this.value);
	        } else {
	            AddToTreatmentsTotal(this.value);
	        }
	    });

	    $(document).on('focusin', 'input[name="Treatment_Cost"]', function () {
	        if (this.value != undefined && this.value != "") {
	            console.log(this.value);
	            treatmentFocusValue = this.value;
	        }
	    });

	    //End Treatment section


	    //Class section
	    $("#btnAddClassRow").on("click", function () {
	        $('#classTable > tbody:last-child').append("<tr>" +
	            '<td class="gf-table-m"><input type="text" name="Class_GuestName" /></td>' +
                '<td class="gf-table-xl"><input type="text" name="Class_Class" /></td>' +
                '<td class="gf-table-date"><input type="text" class="date" name="Class_Date" /></td>' +
                '<td class="gf-table-s"><input type="text" name="Class_Time" /></td>' +
                '<td class="gf-table-s"><input type="text" name="Class_Cost" data-costgroup="Treatment" /></td>' +	            
	         "</tr>");
	    });

	    var classCostFocusValue;
	    $(document).on('change', 'input[name="Class_Cost"]', function () {
	        alert(this.value);
	        if (classCostFocusValue != "" && classCostFocusValue != undefined && this.value != "" && this.value != undefined) {
	            RemoveFromTreatmentsTotal(classCostFocusValue);
	            AddToTreatmentsTotal(this.value);
	        } else {
	            AddToTreatmentsTotal(this.value);
	        }
	    });

	    $(document).on('focusin', 'input[name="Class_Cost"]', function () {
	        if (this.value != undefined && this.value != "") {
	            console.log(this.value);
	            classCostFocusValue = this.value;
	        }
	    });

	    //End Class section

	    //Submit Form

	    $('#GuestForm').on('submit', function (e) {
	        e.preventDefault();
	        $.ajax({
	            url: '/Umbraco/surface/Form/SendContactFormData',
	            type: "POST",
	            data: $(this).serialize(),
	            success: function (data) {
	                $("#form_output").html(data);
	            },
	            error: function (jXHR, textStatus, errorThrown) {
	                alert(errorThrown);
	            }
	        });
	    });
	}
});

