﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using umbraco;
using Umbraco.Web.Mvc;

namespace EndzoneUmbracoTest.Controllers
{
    public class OfferController : SurfaceController
    {
        public ActionResult GetOffersByPageNumber(int pn, int pageSize)
        {
            int previousPage = pn - 1;

            List<Umbraco.Core.Models.IPublishedContent> offerList = UmbracoContext.ContentCache.GetById(uQuery.GetNodesByType("OffersList").FirstOrDefault().Id).Children.OrderByDescending(x => x.CreateDate).Skip(previousPage * pageSize).Take(pageSize).ToList();
            TempData["previousPage"] = previousPage;
            TempData["currentPage"] = pn;
            int totalNumber = GetTotalOffersCount();
            TempData["pageNumber"] = GetNumberOfPagesInList(totalNumber, pageSize);

            return PartialView("OfferListPartial", offerList);
        }

        public int GetTotalOffersCount()
        {
            return UmbracoContext.ContentCache.GetById(uQuery.GetNodesByType("OffersList").FirstOrDefault().Id).Children.Count();
        }

        public int GetNumberOfPagesInList(double totalOffersCount, double pageSize)
        {
            if (totalOffersCount != 0)
                return Convert.ToInt32((totalOffersCount / pageSize) + 0.5);
            else
                return 0;
        }

    }
}