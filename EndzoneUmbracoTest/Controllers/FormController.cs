﻿using System;
using System.Linq;
using System.Net.Http;
using System.Net.Mail;
using System.Web.Mvc;
using SendGrid;
using umbraco;
using Umbraco.Web;
using Umbraco.Web.Mvc;

namespace EndzoneUmbracoTest.Controllers
{
    public class FormController : SurfaceController
    {
        public ActionResult SendContactFormData(FormCollection collection)
        {
            Umbraco.Core.Models.IPublishedContent emailForm = UmbracoContext.ContentCache.GetById(uQuery.GetNodesByType("EmailForm").FirstOrDefault().Id);

            string temp = collection["Resort"];
            string temp2 = collection["ReferenceNum"];

            string subject = "";
            if (emailForm.HasValue("subject"))
            {
                subject = emailForm.GetPropertyValue<string>("subject");
            }
            string toAddress = "";

            if (emailForm.HasValue("toAddress"))
            {
                toAddress = emailForm.GetPropertyValue<string>("toAddress");
            }
            string thankYouMessage = "";
            if (emailForm.HasValue("thankYouMessage"))
            {
                thankYouMessage = emailForm.GetPropertyValue<string>("thankYouMessage");
            }


            SendGridMessage myMessage = new SendGridMessage();
            myMessage.AddTo(toAddress);
            myMessage.From = new MailAddress("john@example.com", "John Smith");
            myMessage.Subject = subject;
            myMessage.Text = "Hello World!";

            // Create a Web transport, using API Key
            var transportWeb = new Web("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");
            try
            {
                // Send the email.
                transportWeb.DeliverAsync(myMessage).Wait();
                return PartialView("EmailFormResultPartial", thankYouMessage);
            }
            catch (Exception ex)
            {
                return PartialView("EmailFormResultPartial", "Something went wrong!");
            }



        }
    }
}